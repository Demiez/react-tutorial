import React, {Component} from 'react'
import Table from './Table'


class App extends Component {
    constructor (props) {
        super(props);
        this.state = {
            characters: [
                {
                    name: 'Charlie',
                    job: 'Janitor'
                },
                {
                    name: 'Mac',
                    job: 'Bouncer'
                },
                {
                    name: 'Dee',
                    job: 'Aspring Actress'
                },
                {
                    name: 'Dennis',
                    job: 'Bartender'
                },
            ]
        }
    }

    render() {

        this.removeCharacter = (row,index) => {
            let charectersCopy = [ ...this.state.characters]
            charectersCopy = charectersCopy.filter((character) => character !== row)
            console.log(charectersCopy)
            console.log(row)


            this.setState({
                characters: charectersCopy
            }, () => console.log(this.state.characters))
            console.log(this.state.characters)


            // await this.setState({
            //     characters: characters.filter((character) => character !== row)
            //     // characters: characters.map((x, i) => {
            //     //    console.log(x,i)
            //     //    return i !== index ? x : null
            //     // })
            // })
            // console.log(this.state.characters)
        }

        // const names = ['John', 'Bob', 'Marv', 'Ted']
        return (
            <div className="container">
                <Table characterData={this.state.characters} removeCharacter={this.removeCharacter} />
            </div>
        )
    }
}

export default App
